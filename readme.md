# .NET performance using Span, Memory etc.

Personal development looking at performance enhancements to .NET.
Also did check up on some other new features in .NET Core 3.x.

## Project setup

1. Get project: **git clone https://gitlab.com/mattincode/dotnet-performance.git**
2. Open project using VS Code or Visual Studio

### Goals:

- Project setup using .NET Core 3.1 (C#8). Also add "unsafe" for future test-code.
- Setup simple example using stacalloc and Span (instance equality).
- Unmanaged memory interop using Memory and Span.
- Watch High Perf C# seminar: https://www.youtube.com/watch?v=NVWQRbqcXJ4&t=3025s&ab_channel=Pusher

### Notes:

- **Span<<T>T</T>>** lives on the stack and is implemented as:

```
public readonly ref struct Span<T>
{
  private readonly ref T _pointer;
  private readonly int _length;
  ...
}
```

- **Memory<<T>T</T>>** lives on the heap and thus has more overhead than Span but can be used in async scenarios etc. where heap-allocation is nescessary. Thread safe!
- There are read-only variants of both Span and Memory that can be used to enforce immutability.
- Benchmarking lib: https://benchmarkdotnet.org/
- Regardless of performance enhancements, the most important is to build software architecture based on specific use cases:
  - Setup data-structures to match the use case and minimize cache misses (Data Oriented).
  - By using a Data Oriented approach the execution of operations on large amounts of data is order of magnitudes faster.
  - This is not "pre mature optimization". If the software architecture is built poorly from the start there is no simple way to improve performance. see: https://www.youtube.com/watch?v=rX0ItVEVjHc&list=PLEektzYBNwf4tDXsJUrRoxTygC31pXQSo&index=7&t=1271s&ab_channel=CppCon
- **System.IO.Pipelines** is used in Kestrel to improve performance, two sides of a pipe: PipeWriter and PipeReader. Managers buffers internally. -> new Pipe()... read and write can be done at the same time!
- **System.Text.Json** new in .NET Core 3.0 -> Lowlevel UTF8JsonReader/Writer,... HighLevel JsonSerializer.
  - This is based on JSON.NET and is sadly a typical case where an external library-manager (James Newton-King) reaches the end of the line of what can be done from the outside. In this case he joined Microsoft.
- **gRPC:** https://github.com/grpc/grpc-dotnet included in .NET Core 3.0
  - Have looked at this before and yes this can be a useful when providing and consuming "public" api:s. This was originally created by Google, based on Protocol Buffers(ProtoBuf) as description.
  - This kind of "standards" seems to die after a short while, see SOAP and then WCF... in this case though more than one of the major software giants seems to have adopted it.
  - However for use inside an application (server - client) it may be overkill, go for something simpler.. preferrably binary in production-builds that optimize performance.
