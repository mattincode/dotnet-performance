﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

namespace dotnet_performance
{
  struct Position
  {
    public int X;
    public int Y;
    public int Z;
  }

  class Program
  {
    static void Main(string[] args)
    {
      // *** Span example 1 - equality
      Span<Position> entityPositions = stackalloc[]
      {
        new Position { X = 1, Y = 0, Z = 0 },
        new Position { X = 0, Y = 1, Z = 0 },
        new Position { X = 1, Y = 1, Z = 0 }
      };
      // We can make the acessor read-only if we need to enforce immutability 
      ReadOnlySpan<Position> pointerIntoPos = entityPositions.Slice(1);
      // We just verify that it is the same instances... the use case here is to have a pointer-like access which is very useful/fast when parsing large chunks of data.
      var isSame = entityPositions[1].Equals(pointerIntoPos[0]);
      Console.WriteLine($"Example1 - isSame: {isSame}");
      // *** Memory example - native memory interop
      int Mem_ALLOC_BYTES = 100 * Unsafe.SizeOf<Position>();
      var unmanagedMemory = Marshal.AllocHGlobal(Mem_ALLOC_BYTES);
      // Let the GC know that there is unmanaged memory associated
      // This is so that the GC can clean this up if we for example deallocate the memory on Dispose...
      GC.AddMemoryPressure(Mem_ALLOC_BYTES); // Needs to be paired with "GC.RemoveMemoryPressure"
      // Below is mainly to use the unmanged memory (should be encapsulated)      
      unsafe
      {
        // Create a span to access part of the unmanaged memory in a typesafe way
        var span = new Span<Position>(unmanagedMemory.ToPointer(), 10);
        span[0] = new Position { X = 1, Y = 2, Z = 3 };
      }
      Marshal.FreeHGlobal(unmanagedMemory);
      GC.RemoveMemoryPressure(Mem_ALLOC_BYTES);
    }
  }
}